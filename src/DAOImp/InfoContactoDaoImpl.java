package DAOImp;



import DAOlocal.InfoContatoDaoLocal;
import model.InfoContacto;

import util.conexion;

public class InfoContactoDaoImpl implements InfoContatoDaoLocal
{
	conexion cn = new conexion();


	@Override
	public void RegistrarInfoContacto(InfoContacto obj) throws Exception {
		
		cn.Abrir_Conexion();
		
		cn.em.getTransaction().begin();
		cn.em.persist(obj);
		cn.em.getTransaction().commit();

		cn.Cerrar_Conexion();

		
	}


	@Override
	public boolean ActualizarInfoContacto(InfoContacto obj) throws Exception 
	{
		cn.Abrir_Conexion();
		
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(obj);
			cn.em.getTransaction().commit();
			return true;
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
			
		}
		finally {
			cn.Cerrar_Conexion();
		}
	
	}
	
}
