package DAOImp;

import java.util.List;

import javax.persistence.Query;

import DAOlocal.ProveedorDAOLocal;
import model.Producto;
import model.Proveedor;
import model.Proveedor;
import util.conexion;

public class ProveedorDAOImp implements ProveedorDAOLocal
{
	conexion cn = new conexion();
	@Override
	public boolean registrar(Proveedor proveedor) throws Exception 
	{
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(proveedor);
			cn.em.getTransaction().commit();
			return true;
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.Cerrar_Conexion();
		}
		
	}
	
	@Override
	public List<Proveedor> listar() throws Exception 
	{
		try 
		{
			cn.Abrir_Conexion();
			List<Proveedor> lista = cn.em.createQuery("select a from Proveedor a").getResultList();
			return lista;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally {
			cn.Cerrar_Conexion();
		}
		
	}
	
	@Override
	public boolean actualizar(Proveedor proveedor) throws Exception 
	{
		cn.Abrir_Conexion();
		
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(proveedor);
			cn.em.getTransaction().commit();
			return true;
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally {
			cn.Cerrar_Conexion();
		}
		
	}

	@Override
	public Proveedor Buscar(String id) throws Exception {
		
		cn.Abrir_Conexion();
		Proveedor pro = new Proveedor();
		try {
			cn.em.getTransaction().begin();
			pro = cn.em.find(Proveedor.class, Integer.parseInt(id));
			cn.em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cn.Cerrar_Conexion();
		}

		return pro;
		
	}

	@Override
	public List<Proveedor> Listar(String o) throws Exception {
		cn.Abrir_Conexion();
		List<Proveedor> proveedores = null;
		try {
			Query query = cn.em.createQuery("SELECT p FROM Proveedor p where p.razonSocial like '%" + o + "%'");

			proveedores = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cn.Cerrar_Conexion();
		}
		return proveedores;
	}
}	
