package DAOImp;

import java.util.List;

import javax.persistence.Query;

import DAOlocal.cargoDAOLocal;
import model.Cargo;
import util.conexion;

public class cargoDAOImp implements cargoDAOLocal 
{
	conexion cn = new conexion();
	
	
	@Override
	public boolean registrar(Cargo cargo) throws Exception 
	{
		cn.Abrir_Conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(cargo);
			cn.em.getTransaction().commit();
			return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally 
		{
			cn.Cerrar_Conexion();
		}
	}
	
	@Override
	public List<Cargo> listar() throws Exception 
	{
		cn.Abrir_Conexion();
		try 
		{
			Query query = cn.em.createQuery("select c from Cargo c");
			return query.getResultList();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally {
			cn.Cerrar_Conexion();
		}
	}

	@Override
	public boolean actualizar(Cargo cargo) throws Exception 
	{
		cn.Abrir_Conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(cargo);
			cn.em.getTransaction().commit();
			return true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			cn.Cerrar_Conexion();
		}
	}
}
