package DAOImp;

import java.util.List;

import javax.persistence.Query;

import DAOlocal.MetodosGenericosJPQLDAOLocal;
import util.conexion;

public class MetodosGenernicosJPQLimp implements MetodosGenericosJPQLDAOLocal {
conexion cn = new conexion();
	
	@Override
	public List ListarGenerico(String JPQLQuery) throws Exception {
		
		cn.Abrir_Conexion();
		List list=null;
	try 
		{
			Query query  = cn.em.createNativeQuery(JPQLQuery);
			list=query.getResultList();
		} 
	catch (Exception e) 
		{
			e.printStackTrace();
		}
	finally 
		{
			cn.Cerrar_Conexion();
		}
	return list;
	}

	@Override
	public boolean InsertGenerico(String JPQLQUERY) throws Exception {
		cn.Abrir_Conexion();
		
		try 
		{
			
			cn.em.getTransaction().begin();
			Query query = cn.em.createNativeQuery(JPQLQUERY);
			query.executeUpdate();
			cn.em.getTransaction().commit();
			
			
			return true;
		} catch (Exception e) 
		{
			return false;
		}
		finally 
		{
		cn.Cerrar_Conexion();	
		}
		
	}
	
	

}
