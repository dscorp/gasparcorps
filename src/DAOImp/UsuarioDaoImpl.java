package DAOImp;
import java.util.List;
import DAOlocal.UsuarioDaoLocal;

import javax.persistence.ParameterMode;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import model.Usuario;
import util.conexion;

public class UsuarioDaoImpl implements UsuarioDaoLocal
{
	conexion cn = new conexion();
	
	@Override
	public boolean agregarUsuario(Usuario obj) throws Exception 
	{
		
		cn.Abrir_Conexion();
			
		try 
		{		
			cn.em.getTransaction().begin();
			cn.em.persist(obj);
			cn.em.getTransaction().commit();
			
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
		finally
		{
			cn.Cerrar_Conexion();
		}
		
	}
	
	@Override
	public List<Usuario> Listar() throws Exception 
	{
		
		try {
			cn.Abrir_Conexion();
			Query query = cn.em.createQuery("select u from Usuario u");
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			cn.Cerrar_Conexion();
		}
		
		
		
		
	}

	@Override
	public boolean ActualizarUsuario(Usuario obj) throws Exception {
	
		cn.Abrir_Conexion();
		
		try 
		{
			Usuario usuario = cn.em.find(Usuario.class, obj.getIdUsuario());
			usuario.setContrasena(obj.getContrasena());
			cn.em.getTransaction().begin();
			cn.em.merge(obj);
			cn.em.getTransaction().commit();
			return true;
			
			
		} 
	catch (Exception e) 
		{
			e.printStackTrace();
		}
	finally 
		{
			cn.Cerrar_Conexion();
		}
		return false;
	
	}

	@Override
	public boolean IniciarSession(Usuario obj) throws Exception {
		// TODO Auto-generated method stub
		try {
			cn.Abrir_Conexion();
			
			
			Query q=cn.em.createQuery("select u from Usuario u inner join u.tblEmpleado e inner join e.tblcargo  where u.usuario='"+obj.getUsuario()+"' and u.contrasena='"+obj.getContrasena()+"'");
			Usuario usu =(Usuario) q.getSingleResult();
			
			if(usu.getIdUsuario()!=0)
			{

				System.out.println("UsuarioDaoImpl.IniciarSession()---true");
				return true;
			}
			else

				System.out.println("UsuarioDaoImpl.IniciarSession()---false");
				return false;
//		
//			StoredProcedureQuery  query = cn.em.createStoredProcedureQuery("IniciarSession");
//			query.registerStoredProcedureParameter("usuariop", String.class, ParameterMode.IN);
//			query.registerStoredProcedureParameter("contrasenap", String.class, ParameterMode.IN);
//			query.setParameter("usuariop", obj.getUsuario());
//			query.setParameter("contrasenap", obj.getContrasena());
					
//			return (query.getResultList().size() > 0);
		} catch (Exception e) {
		// TODO: handle exception
			e.printStackTrace();
		
				return false;
		}finally {
			cn.Cerrar_Conexion();
		}	
	}
}
