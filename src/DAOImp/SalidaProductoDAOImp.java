package DAOImp;

import java.util.List;

import DAOlocal.EntradaProductoDAOLocal;
import DAOlocal.SalidaProductoDAOLocal;
import model.EntradaProducto;
import model.SalidaProducto;
import model.empleado;
import util.conexion;

public class SalidaProductoDAOImp implements SalidaProductoDAOLocal
{
	conexion cn = new conexion();
	@Override
	public boolean registrar(SalidaProducto salidaProducto) throws Exception 
	{
		cn.Abrir_Conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(salidaProducto);
			cn.em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public List<empleado> listarE() throws Exception
	{
		cn.Abrir_Conexion();
		List<empleado> lista = cn.em.createQuery("select a from empleado a").getResultList();
		return lista;
	}

	@Override
	public List<SalidaProducto> listar() throws Exception {
		try {
			

			cn.Abrir_Conexion();
			
			List<SalidaProducto> lista = cn.em.createQuery("select s from SalidaProducto s").getResultList();
			return lista;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			cn.Cerrar_Conexion();
		}
	}
	
}
