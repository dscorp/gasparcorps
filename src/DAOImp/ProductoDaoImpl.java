package DAOImp;

import java.math.BigDecimal;
import java.util.ArrayList;

import java.util.List;

import DAOlocal.ProductoDaoLocal;
import javax.persistence.Query;

import model.DetalleProducto;
import model.Producto;
import model.ProductoProveedor;
import model.Proveedor;
import util.RepSalidasYear;
import util.conexion;

public class ProductoDaoImpl implements ProductoDaoLocal {
	conexion cn = new conexion();

	@Override
	public void agregarProducto(Producto obj) throws Exception {

		cn.Abrir_Conexion();
try {
	cn.em.getTransaction().begin();
	cn.em.persist(obj);
	cn.em.getTransaction().commit();
} catch (Exception e) {
	e.printStackTrace();
}
		
			finally {
				cn.Cerrar_Conexion();
			}
		

	}

	@Override
	public List<Producto> Listar() throws Exception {

		try {
			cn.Abrir_Conexion();
			Query query = cn.em.createQuery(
					"SELECT di,u FROM Producto di INNER JOIN di.tblDetPro u WHERE u.estado =:e order by di.idProducto asc");
			query.setParameter("e", 1);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("ocurrio un error");
			return null;
		} finally {
			cn.Cerrar_Conexion();
		}

	}

	@Override
	public boolean ActualizarProducto(DetalleProducto dp, BigDecimal NPrecio) throws Exception {

		cn.Abrir_Conexion();
		boolean actualizo = false;
		try {
			cn.em.getTransaction().begin();
			dp.setEstado(0);
			cn.em.merge(dp);

			Producto prod = dp.getTblProducto();
			DetalleProducto dprod = new DetalleProducto();

			dprod.setPrecio(NPrecio);
			dprod.setTblProducto(prod);
			dprod.setEstado(1);
//			dprod.setFecha(java.util.Calendar.getInstance().getTime());
			cn.em.merge(prod);
			cn.em.persist(dprod);
			cn.em.getTransaction().commit();
			actualizo = true;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cn.Cerrar_Conexion();
		}
		return actualizo;

	}

	@Override
	public boolean agregarIntermedia(ProductoProveedor obj) throws Exception {
		conexion cn = new conexion();

		cn.Abrir_Conexion();

		try {
			cn.em.getTransaction().begin();
			cn.em.persist(obj);
			cn.em.getTransaction().commit();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		finally {
			cn.Cerrar_Conexion();
		}

	}

	@Override
	public List<Proveedor> mlpf() throws Exception {
		try {
			cn.Abrir_Conexion();
			Query query = cn.em.createNativeQuery(
					"select p.idProveedor, p.Ciudad, p.RazonSocial, p.Ruc, p.fechaRegistro, p.idInfoContacto from proveedor p left OUTER JOIN productoproveedor pp on p.idProveedor=pp.idProveedor where pp.idProducto is null");
			List<Proveedor> xs = new ArrayList<>();
			List<Object[]> proveedores = query.getResultList();

			for (Object[] a : proveedores) {
				Proveedor pr = new Proveedor();
				pr.setIdProveedor(Integer.parseInt(a[0] + ""));
				pr.setCiudadx(a[1] + "");
				pr.setRazonSocial(a[2] + "");
				pr.setRuc(a[3] + "");

				pr.setIdProveedor(Integer.parseInt(a[5] + ""));
				xs.add(pr);
			}

			return xs;

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("ocurrio un error");
			return null;
		} finally {
			cn.Cerrar_Conexion();
		}
	}

	@Override
	public List<Producto> ListarSinDetalle() throws Exception {
		try {
			cn.Abrir_Conexion();
			Query query = cn.em.createQuery("SELECT p from Producto p");

			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("ocurrio un error");
			return null;
		} finally {
			cn.Cerrar_Conexion();
		}

	}

	@Override
	public Producto Buscar(String id) throws Exception {
		cn.Abrir_Conexion();
		Producto pro = new Producto();
		try {
			cn.em.getTransaction().begin();
			pro = cn.em.find(Producto.class, Integer.parseInt(id));
			cn.em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cn.Cerrar_Conexion();
		}

		return pro;
	}

	@Override
	public List<Producto> Listar(String o) throws Exception {
		cn.Abrir_Conexion();
		List<Producto> productos = null;
		try {
			Query query = cn.em.createQuery("SELECT p FROM Producto p where p.nombre like '%" + o + "%'");

			productos = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cn.Cerrar_Conexion();
		}
		return productos;
	}

	@Override
	public boolean Eliminar(Producto o) throws Exception {
		try {
			cn.Abrir_Conexion();
			Producto p = cn.em.find(Producto.class, o.getIdProducto());
			cn.em.getTransaction().begin();
			cn.em.remove(p);
			cn.em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			cn.Cerrar_Conexion();
		}
	}

	@Override
	public List<Producto> ListarCondicionado(String c) throws Exception {
		cn.Abrir_Conexion();
		List<Producto> productos = null;
		try {
			String q = "SELECT p FROM Producto p " + c;
			Query query = cn.em.createQuery(q);
			System.out.println(q);

			productos = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cn.Cerrar_Conexion();
		}
		return productos;
	}

	@Override
	public List<RepSalidasYear> SalidasAnio(String y) throws Exception {
		cn.Abrir_Conexion();
		List<RepSalidasYear> salidas = new ArrayList<>();
		try {
			String q = "select DATE_FORMAT(sp.fechaSalida,'%m/%Y') as mes, count(sp.idSalidaProducto) as concurrencia from salidaproducto sp where YEAR(sp.fechaSalida) = '"
					+ 2018 + "' GROUP BY DATE_FORMAT(sp.fechaSalida,'%m/%Y') ";

			System.out.println(q);
			Query query = cn.em.createNativeQuery(q);
			
			List<Object[]> res = query.getResultList();
System.out.println(res.size());
			for (Object[] a : res) {
				RepSalidasYear pr = new RepSalidasYear();
				pr.setMes((a[0] + ""));
				pr.setConcurrencia(Integer.parseInt(a[1] + ""));
				salidas.add(pr);
				System.out.println(pr.getMes()+" "+pr.getConcurrencia());
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cn.Cerrar_Conexion();
		}
		return salidas;
	}

	@Override
	public void ActualizarSinDetalle(Producto obj) throws Exception{
		try 
		{
			cn.Abrir_Conexion();
			cn.em.getTransaction().begin();
			cn.em.merge(obj);
			cn.em.getTransaction().commit();
		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			cn.Cerrar_Conexion();
		}
		
	}
}
