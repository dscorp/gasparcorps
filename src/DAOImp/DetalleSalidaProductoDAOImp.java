package DAOImp;

import java.util.List;

import DAOlocal.DetalleEntradaProductoDAOLocal;
import DAOlocal.DetalleSalidaProductoDAOLocal;
import model.DetalleSalidaProducto;
import model.Producto;
import model.empleado;
import util.conexion;

public class DetalleSalidaProductoDAOImp implements DetalleSalidaProductoDAOLocal
{
	conexion cn = new conexion();
	
	@Override
	public List<Producto> listarP() throws Exception
	{
		cn.Abrir_Conexion();
		List<Producto> lista = cn.em.createQuery("select a from Producto a").getResultList();
		return lista;
	}
	
	@Override
	public List<DetalleSalidaProducto> listar() throws Exception 
	{
		cn.Abrir_Conexion();
		List<DetalleSalidaProducto> lista = cn.em.createQuery("select a from DetalleSalidaProducto a").getResultList();
		return lista;
	}
}
