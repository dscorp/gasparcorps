package DAOImp;


import java.util.List;

import javax.persistence.Query;

import DAOlocal.CategoriaDAOLocal;
import model.Categoria;
import util.conexion;

public class CategoriaDAOImpl implements CategoriaDAOLocal 
{
	conexion cn = new conexion();
	@Override
	public boolean Registrar(Categoria categoria) throws Exception 
	{
		cn.Abrir_Conexion();
		
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(categoria);
			cn.em.getTransaction().commit();
			return true;
		} catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		
		finally
		{
			cn.Cerrar_Conexion();
		}
	}
	
	@Override
	public List<Categoria> listar() throws Exception 
	{
		cn.Abrir_Conexion();
		try 
		{
			cn.em.getTransaction();
			List<Categoria> listacat = cn.em.createQuery("select c from Categoria c ").getResultList();
			return listacat;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally {
			cn.Cerrar_Conexion();
		}
	}
	
	@Override
	public boolean Actualizar(Categoria categoria) throws Exception 
	{
		
		cn.Abrir_Conexion();
		
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(categoria);
			cn.em.getTransaction().commit();
			return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		
		finally
		{
			cn.Cerrar_Conexion();
		}
	}
}
