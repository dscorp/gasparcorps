package DAOImp;

import java.util.List;

import DAOlocal.EntradaProductoDAOLocal;
import model.EntradaProducto;
import model.empleado;
import util.conexion;

public class EntradaProductoDAOImp implements EntradaProductoDAOLocal
{
	conexion cn = new conexion();
	@Override
	public boolean registrar(EntradaProducto entradaProducto) throws Exception 
	{
		cn.Abrir_Conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.persist(entradaProducto);
			cn.em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public List<empleado> listarE() throws Exception
	{
		cn.Abrir_Conexion();
		List<empleado> lista = cn.em.createQuery("select a from empleado a").getResultList();
		return lista;
	}

	@Override
	public List<EntradaProducto> listar() throws Exception
	{
		cn.Abrir_Conexion();
		List<EntradaProducto> lista = cn.em.createQuery("select a from EntradaProducto a").getResultList();
		return lista;
	}
	
	
	
}
