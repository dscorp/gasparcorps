package DAOImp;

import java.util.List;

import DAOlocal.DetalleEntradaProductoDAOLocal;
import model.DetalleEntradaProducto;
import model.DetalleSalidaProducto;
import model.Producto;
import model.empleado;
import util.conexion;

public class DetalleEntradaProductoDAOImp implements DetalleEntradaProductoDAOLocal
{
	conexion cn = new conexion();
	
	@Override
	public List<Producto> listarP() throws Exception
	{
		cn.Abrir_Conexion();
		List<Producto> lista = cn.em.createQuery("select a from Producto a").getResultList();
		return lista;
	}
	
	@Override
	public List<DetalleEntradaProducto> listar() throws Exception 
	{
		cn.Abrir_Conexion();
		List<DetalleEntradaProducto> lista = cn.em.createQuery("select a from DetalleEntradaProducto a").getResultList();
		return lista;
	}
	
	@Override
	public boolean actualizar(DetalleEntradaProducto detEntPro) throws Exception 
	{
		cn.Abrir_Conexion();
		try 
		{
			cn.em.getTransaction().begin();
			cn.em.merge(detEntPro);
			cn.em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}
}
