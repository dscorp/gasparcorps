package DAOImp;

import java.util.List;

import javax.persistence.Query;

import DAOlocal.empleadoDAOLocal;
import model.Cargo;
import model.empleado;
import util.conexion;

public class empleadoDAOImp implements empleadoDAOLocal {
	conexion cn = new conexion();

	@Override
	public boolean Registrar(empleado empleado) throws Exception {
		cn.Abrir_Conexion();
		

		try {
			cn.em.getTransaction().begin();
			cn.em.persist(empleado);
			cn.em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		} finally {
			cn.Cerrar_Conexion();
		}

		
	}

	@Override
	public List<empleado> listar() throws Exception 
	{
		try {
				

			cn.Abrir_Conexion();
			
			List<empleado> lista = cn.em.createQuery("select a from empleado a").getResultList();
			return lista;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			cn.Cerrar_Conexion();
		}
		
	}

	@Override
	public List<empleado> ListarPorCargo(String o) throws Exception {
		try {
			

			cn.Abrir_Conexion();
			
			 
			Query query=cn.em.createQuery("SELECT e,c FROM empleado e INNER JOIN e.tblcargo c WHERE c.descripcion =:e ");
			query.setParameter("e", o);
			List<empleado> lista=query.getResultList();
			return lista;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			cn.Cerrar_Conexion();
		}
		
	}
	
	@Override
	public List<empleado> listarPorCargo2(String m, String n) throws Exception 
	{
		cn.Abrir_Conexion();
		try 
		{
			Query query=cn.em.createQuery("SELECT e,c FROM empleado e INNER JOIN e.tblcargo c WHERE c.descripcion =:e or c.descripcion =:a");
			query.setParameter("e", m);
			query.setParameter("a", n);
			List<empleado> lista=query.getResultList();
			return lista;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		finally {
			cn.Cerrar_Conexion();
		}
	}
	
	@Override
	public boolean actualizar(empleado empleado) throws Exception
	{
		cn.Abrir_Conexion();
		try 
		{
			Cargo c = cn.em.find(Cargo.class, empleado.getTblcargo().getIdCargo());
			empleado.setTblcargo(c);
			cn.em.getTransaction().begin();
			cn.em.merge(empleado);
			cn.em.getTransaction().commit();
			return true;

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		
		finally
		{
			cn.Cerrar_Conexion();
		}
	}
	
}
