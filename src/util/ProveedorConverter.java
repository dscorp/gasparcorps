package util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import model.Proveedor;

import services.ProveedorService;

@FacesConverter("ProveedorConverter")
public class ProveedorConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        ProveedorService proserv = new ProveedorService();
        Proveedor pro=null;
		try {
			pro = proserv.Buscar(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return pro;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
    	return String.valueOf(((Proveedor) value).getIdProveedor());
    }

}
