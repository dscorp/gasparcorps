package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class conexion {

	public EntityManager em;
	private EntityManagerFactory emf;
		
	public void Abrir_Conexion() throws Exception
	{
		emf=Persistence.createEntityManagerFactory("gasparcorps");
		em=emf.createEntityManager();
	}
	
	public void Cerrar_Conexion() throws Exception
	{
		em.close();
		emf.close();
		
	}
	
	
}
