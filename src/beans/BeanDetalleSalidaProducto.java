package beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;

import model.DetalleEntradaProducto;
import model.DetalleSalidaProducto;
import model.EntradaProducto;
import model.Producto;
import model.SalidaProducto;
import model.empleado;
import services.DetalleEntradaProductoService;
import services.DetalleSalidaProductoService;
import services.EntradaProductoService;
import services.ProductoService;
import services.SalidaProductoService;
import services.empleadoService;

@ManagedBean(name="BeanDetalleSalidaProducto")
@ViewScoped

public class BeanDetalleSalidaProducto
{
	private DetalleSalidaProducto detSalPro= new DetalleSalidaProducto();
	private SalidaProducto salPro = new SalidaProducto();
	private Producto prod= new Producto();
	private empleado emp=new empleado();
	
	private DetalleSalidaProductoService detSalProService = new DetalleSalidaProductoService();
	private SalidaProductoService salProService = new SalidaProductoService();
	
	private List<Producto> listaProducto=new ArrayList<>();
	private List<empleado> listaEmpleado = new ArrayList<>();
	private empleadoService empserv = new empleadoService();
	private ProductoService proserv = new ProductoService();
	private List<DetalleSalidaProducto> listaDetSalPro;
	private List<SalidaProducto>  outs = new ArrayList<>();
	private List<DetalleSalidaProducto> dets = new ArrayList<>();
	private List<DetalleSalidaProducto> detsselected = new ArrayList<>();
	
	public BeanDetalleSalidaProducto() throws Exception
	{

		listarSalidaProducto();
		 ListarEmpleados();
		 ListarProductos();
	}

	public void SelectedOutDetails(List<DetalleSalidaProducto> o) {
		detsselected=o;
	}
	

	public void ListarEmpleados () throws Exception
	{
		listaEmpleado =empserv.listar();
	}
	
	
	public void ListarProductos() throws Exception
	{
		listaProducto = proserv.ListarSinDetalle();
	}

	
	public void adddet() 
	{
		detSalPro.setTblSalPro(salPro);
		
		if(dets.isEmpty())
		{
			if(detSalPro.getCantidad()==0)
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad > a 0",""));
			}
			else 
			{
				
				if(detSalPro.getCantidad() > Double.parseDouble(detSalPro.getTblProducto().getStock()+""))
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: No hay stock suficiente para el producto"+detSalPro.getTblProducto().getNombre()+" solo se cuenta con "+detSalPro.getTblProducto().getStock() +"unidades",""));
				}
				else 
				{
					dets.add(detSalPro);
				}
					
			}
			
		}
		else
		{
			boolean existe=false;
			int index=0;
			for (int i = 0; i < dets.size(); i++) 
			{
				if(detSalPro.getTblProducto().getCodigoBarra().equals(dets.get(i).getTblProducto().getCodigoBarra()))	
				{
					existe=true;
					index=i;
				}
			}
			if(existe)
			{
				if(detSalPro.getCantidad()==0)
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad > a 0",""));
				
				}
				else
				{
					
					if((dets.get(index).getCantidad() + detSalPro.getCantidad()) > Double.parseDouble(dets.get(index).getTblProducto().getStock()+""))
					{
						FacesContext.getCurrentInstance().addMessage
						(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: No hay stock suficiente para el producto"+detSalPro.getTblProducto().getNombre()+" solo se cuenta con "+detSalPro.getTblProducto().getStock() +"unidades",""));
					}
					else 
					{
						dets.get(index).setCantidad(dets.get(index).getCantidad() + detSalPro.getCantidad());
						FacesContext.getCurrentInstance().addMessage
						(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"El producto ya existe, Se agregaron " + detSalPro.getCantidad() + "Unidades", ""));
						
					}
					
					
					
				}
				{
					
				}
			}
			else 
			{
				if(detSalPro.getCantidad()==0)
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad > a 0",""));
				}
				else
				{
					if(detSalPro.getCantidad() > Double.parseDouble(detSalPro.getTblProducto().getStock()+""))
					{
						FacesContext.getCurrentInstance().addMessage
						(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: No hay stock suficiente para el producto"+detSalPro.getTblProducto().getNombre()+" solo se cuenta con "+detSalPro.getTblProducto().getStock() +"unidades",""));
					}
					else 
					{
						
						dets.add(detSalPro);
					}
				}
				
			}
		}
		
		
		detSalPro = new DetalleSalidaProducto();
	}
	
//	public void adddet () 
//	{
//		detSalPro.setTblSalPro(salPro);
//		dets.add(detSalPro);
//		
//		
//		detSalPro= new DetalleSalidaProducto();
//	}

	public void deldet (DetalleSalidaProducto o) 
	{
		dets.remove(o);
	}
	
	
	
	
	
	
	public void RegistrarSalida() throws Exception
	{
		if (dets.size()>0 && emp!= null) 
		{
			salPro.setTblDetSalPro(dets);
			
			salPro.setFechaSalida(new Date());
			
			emp.setTblSalPro(new ArrayList<SalidaProducto>(Arrays.asList(salPro)));
			salPro.setTblEmpleado(emp);
			
			
			if (salProService.registrar(salPro))
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"OK Salida Registrada correctamente",""));
//				RequestContext.getCurrentInstance().execute("PF('dialogoRegistrar').hide()");
//				RequestContext.getCurrentInstance().update("tablaSal");
				//esto recorre la lista de detalles y por cada elemento actualiza el stock
				for (DetalleSalidaProducto detalleSalidaProducto  : dets) 
				{
					//aqui se asigna el nuevo stock a cada uno de los productos en la lista de detalles
					detalleSalidaProducto.getTblProducto().setStock(detalleSalidaProducto.getTblProducto().getStock().subtract(new BigDecimal(detalleSalidaProducto.getCantidad())));
					
					//luego se procede a actualizar cada 1
					proserv.ActualizarSinDetalle(detalleSalidaProducto.getTblProducto());
				}
			} 
			else
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Salida No Registrada",""));
			}
			
			prod = new Producto();
			detSalPro = new DetalleSalidaProducto();
			salPro = new SalidaProducto();
			emp = new empleado();
			dets = new ArrayList<>();
			listarSalidaProducto();
		} 
		else 
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe rellenar todos los campos",""));
		}
		
		
		
	}
	
//	public void RegistrarSalida() throws Exception
//	{
//		
//		if(dets.size()>0 && emp!=null)
//		{
//			System.out.println("BeanDetalleSalidaProducto.RegistrarSalida()xxxxxxxxxxxssssssssdtabdtabdtabdtabdtabdtabdtabdtabdtabdtabdtabdtabdtabdtabdtabsssssss");
//			System.out.println(emp.getIdEmpleado());
//			
//			salPro.setFechaSalida(new Date());
//			
//			
//			
////			prod.setTblDetSalPro(new ArrayList<DetalleSalidaProducto>(Arrays.asList(detSalPro)));
////			detSalPro.setTblProducto(prod);
//			
//			salPro.setTblDetSalPro(dets);
//			
//			
//			emp.setTblSalPro(new ArrayList<SalidaProducto>(Arrays.asList(salPro)));
//			salPro.setTblEmpleado(emp);
//			
//			if (salProService.registrar(salPro))
//			{
//				FacesContext.getCurrentInstance().addMessage
//				(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"OK Salida Registrada correctamente",""));
//				RequestContext.getCurrentInstance().execute("PF('dialogoRegistrar').hide()");
//				RequestContext.getCurrentInstance().update("tablaSal");
//				
//				
//			} 
//			else
//			{
//				FacesContext.getCurrentInstance().addMessage
//				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Salida No Registrada",""));
//			}
//			
//			dets = new ArrayList<>();
//			prod = new Producto();
//			salPro = new SalidaProducto();
//			detSalPro = new DetalleSalidaProducto();
//			emp = new empleado();
//			listarSalidaProducto();
//		}
//		
//		else {
//			FacesContext.getCurrentInstance().addMessage
//			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe rellenar todos los campos",""));
//		}
//		
//		
//		
//		
//	}
	
	
	
	public void listarSalidaProducto() throws Exception
	{
		this.outs=salProService.listar();

	}
	
	public void seleccionarSalPro(DetalleSalidaProducto saliPro) throws Exception
	{
		this.detSalPro = saliPro;
	}
	
	public DetalleSalidaProducto getDetSalPro() {
		return detSalPro;
	}


	public void setDetSalPro(DetalleSalidaProducto detSalPro) {
		this.detSalPro = detSalPro;
	}


	public SalidaProducto getSalPro() {
		return salPro;
	}


	public void setSalPro(SalidaProducto salPro) {
		this.salPro = salPro;
	}


	public Producto getProd() {
		return prod;
	}


	public void setProd(Producto prod) {
		this.prod = prod;
	}


	public empleado getEmp() {
		return emp;
	}


	public void setEmp(empleado emp) {
		this.emp = emp;
	}


	public DetalleSalidaProductoService getDetSalProService() {
		return detSalProService;
	}


	public void setDetSalProService(DetalleSalidaProductoService detSalProService) {
		this.detSalProService = detSalProService;
	}


	public SalidaProductoService getSalProService() {
		return salProService;
	}


	public void setSalProService(SalidaProductoService salProService) {
		this.salProService = salProService;
	}


	public List<DetalleSalidaProducto> getListaDetSalPro() {
		return listaDetSalPro;
	}


	public void setListaDetSalPro(List<DetalleSalidaProducto> listaDetSalPro) {
		this.listaDetSalPro = listaDetSalPro;
	}


	public List<Producto> getListaProducto() {
		return listaProducto;
	}


	public void setListaProducto(List<Producto> listaProducto) {
		this.listaProducto = listaProducto;
	}


	public List<empleado> getListaEmpleado() {
		return listaEmpleado;
	}


	public void setListaEmpleado(List<empleado> listaEmpleado) {
		this.listaEmpleado = listaEmpleado;
	}


	public List<DetalleSalidaProducto> getDets() {
		return dets;
	}


	public void setDets(List<DetalleSalidaProducto> dets) {
		this.dets = dets;
	}


	public List<SalidaProducto> getOuts() {
		return outs;
	}


	public List<DetalleSalidaProducto> getDetsselected() {
		return detsselected;
	}


	public void setDetsselected(List<DetalleSalidaProducto> detsselected) {
		this.detsselected = detsselected;
	}


	public void setOuts(List<SalidaProducto> outs) {
		this.outs = outs;
	}



	
	
	
}
