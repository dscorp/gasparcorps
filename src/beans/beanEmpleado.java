package beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;

import model.Cargo;
import model.InfoContacto;
import model.Usuario;
import model.empleado;
import model.persona;
import services.InfoContactoService;
import services.cargoService;
import services.empleadoService;
import util.FinalVariables;

@ManagedBean(name = "beanEmpleado")
@ViewScoped

public class beanEmpleado {
	private empleado empl = new empleado();
	private Cargo carg= new Cargo();
	private persona pers = new persona();
	private InfoContacto inf= new InfoContacto();
	private Usuario usu= new Usuario();
	private cargoService cargservice = new cargoService();
	private empleadoService emplservice = new empleadoService();
	private InfoContactoService infservice = new InfoContactoService();
	
	private List<Cargo> listaTcargo;
	private List<empleado> listaEmpleado;
	private ArrayList<SelectItem> listaCargo;
	private InfoContacto infselcected;

	public beanEmpleado() throws Exception
	{
		listarEmpleados();
		listarCargo();
	}

	
	
	public void SeleccionarInfoContato(InfoContacto obj)
	{
		this.infselcected=obj;
	}
	
	
	
	public ArrayList<SelectItem> getListaCargo() throws Exception
	{
		listaCargo = new ArrayList<SelectItem>();
		for(Cargo obj:cargservice.listar())
		{
			listaCargo.add(new SelectItem(obj.getIdCargo(),obj.getDescripcion()));
		}
		return listaCargo;
	}

	public void setListaCargo(ArrayList<SelectItem> listaCargo) {
		this.listaCargo = listaCargo;
	}




	public void Registrar()
	{
			inf.setPersona(pers);
			pers.setTblInfoContacto(inf);
			
			pers.setTblempleado(empl);
			empl.setTblpersona(pers);
			
			empl.setTblcargo(carg);
			carg.setTblempleado(new ArrayList<empleado>(Arrays.asList(empl)));			
			
			empl.setFechaContrato(new Date());
		
			try {
				
					infservice.RegistrarInfoContacto(inf);
		
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_INFO, "OK: Empleado Registrado", ""));
		
					RequestContext.getCurrentInstance().execute("PF('rprod').hide()");
					RequestContext.getCurrentInstance().update("emmp");
			

				
			} catch (Exception e) {
				e.printStackTrace();
				String ex =e.getCause().getCause().getCause().toString();
				System.out.println(ex);
				
				if(ex.contains(FinalVariables.DUPLICATED)) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR: Empleado no Registrado, ya existe un usuario con este dni", ""));
				
				}
				else {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR: Empleado no Registrado", ""));
				}
			}
			
			
			
			empl = new empleado();
			inf = new InfoContacto();
			carg = new Cargo();
			pers = new persona();
			try {
				listarEmpleados();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	}
	
	public void listarEmpleados() throws Exception
	{
		System.out.println("beanEmpleado.listarEmpleados()yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
		listaEmpleado = emplservice.listar();

		System.out.println("beanEmpleado.listarEmpleados() xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
	}

	public void seleccionarEmpl(empleado emplea) throws Exception
	{
		this.empl = emplea;
	}
	
	public void actualizar () throws Exception
	{
		carg.setTblempleado(new ArrayList<>(Arrays.asList(empl)));
		empl.setTblcargo(carg);
		if (emplservice.actualizar(empl)) 
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK: Empleado Actualizado Correctamente",""));
			RequestContext.getCurrentInstance().execute("PF('dialogoActualizar').hide()");
			RequestContext.getCurrentInstance().update("emmp");
			
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Empleado No Actualizado",""));		
		}
		
		empl = new empleado();
		inf = new InfoContacto();
		pers = new persona();
		carg= new Cargo();
		listarEmpleados();
	}

	
	//// CARGO *///
	public void listarCargo() throws Exception
	{
		listaTcargo = cargservice.listar();
	}
	
	public void seleccionarCargo(Cargo ca) throws Exception
	{
		this.carg = ca;
	}
	

	
	public void registrarCargo() throws Exception
	{
		if (cargservice.registrar(carg))
		{
			FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Cargo Registrado"));
					
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null,
			new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensaje", "Cargo No Registrado"));
		}
		
		carg = new Cargo();
		listarCargo();
	}
	
	////PARA EL CARGO///
	
	public Date getMinAge()
	{
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.YEAR, -18);
		return currentDate.getTime();
		
	}
	
	public Date getMaxAge() 
	{
	    Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.YEAR, -65);
        
        return currentDate.getTime();
	}

	public empleado getEmpl() {
		return empl;
	}

	public void setEmpl(empleado empl) {
		this.empl = empl;
	}

	public Cargo getCarg() {
		return carg;
	}

	public void setCarg(Cargo carg) {
		this.carg = carg;
	}

	public persona getPers() {
		return pers;
	}

	public void setPers(persona pers) {
		this.pers = pers;
	}

	public InfoContacto getInf() {
		return inf;
	}

	public void setInf(InfoContacto inf) {
		this.inf = inf;
	}

	public Usuario getUsu() {
		return usu;
	}

	public void setUsu(Usuario usu) {
		this.usu = usu;
	}

	public empleadoService getEmplservice() {
		return emplservice;
	}

	public void setEmplservice(empleadoService emplservice) {
		this.emplservice = emplservice;
	}

	public InfoContactoService getInfservice() {
		return infservice;
	}

	public void setInfservice(InfoContactoService infservice) {
		this.infservice = infservice;
	}

	public List<empleado> getListaEmpleado() {
		return listaEmpleado;
	}

	public void setListaEmpleado(List<empleado> listaEmpleado) {
		this.listaEmpleado = listaEmpleado;
	}


	public InfoContacto getInfselcected() {
		return infselcected;
	}



	public void setInfselcected(InfoContacto infselcected) {
		this.infselcected = infselcected;
	}



	public cargoService getCargservice() {
		return cargservice;
	}

	public void setCargservice(cargoService cargservice) {
		this.cargservice = cargservice;
	}

	public List<Cargo> getListaTcargo() {
		return listaTcargo;
	}

	public void setListaTcargo(List<Cargo> listaTcargo) {
		this.listaTcargo = listaTcargo;
	}
	
	
}
