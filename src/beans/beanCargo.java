package beans;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import model.Cargo;
import services.cargoService;

@ManagedBean(name = "beanCargo")
@ViewScoped
public class beanCargo {
	private Cargo carg = new Cargo();
	private Cargo actucarg = new Cargo();
	private cargoService cargservice = new cargoService();
	private List<Cargo> listacargada;
	
	public beanCargo() throws Exception
	{

		listar();
	}

	public void listar() throws Exception
	{
		listacargada = cargservice.listar();
	}
	
	public List<Cargo> listarCa() throws Exception
	{
		return cargservice.listar();
	}
	
	
	public void seleccionarCargo(Cargo ca) throws Exception
	{
		this.actucarg = ca;
	}
	
	
	public void registrar() throws Exception
	{
		if (cargservice.registrar(carg))
		{
			listar();
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"OK: Cargo Registrado",""));
			RequestContext.getCurrentInstance().execute("PF('dialogoRegisCargo').hide()");
			RequestContext.getCurrentInstance().update("frm");
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Cargo No Registrado",""));
		}
		
		carg = new Cargo();
	}
	
	public void actualizar() throws Exception
	{
		if (cargservice.actualizar(actucarg))
		{
			listar();
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"OK: Cargo Actualizado ",""));
			RequestContext.getCurrentInstance().execute("PF('dialogoActuCargo').hide()");
			RequestContext.getCurrentInstance().update("frm");
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Cargo No Actualizado",""));
		}
		actucarg=new Cargo();
	}

	public Cargo getCarg() {
		return carg;
	}

	public void setCarg(Cargo carg) {
		this.carg = carg;
	}

	public cargoService getCargservice() {
		return cargservice;
	}

	public void setCargservice(cargoService cargservice) {
		this.cargservice = cargservice;
	}

	public List<Cargo> getListacargada() {
		return listacargada;
	}

	public void setListacargada(List<Cargo> listacargada) {
		this.listacargada = listacargada;
	}

	public Cargo getActucarg() {
		return actucarg;
	}

	public void setActucarg(Cargo actucarg) {
		this.actucarg = actucarg;
	}
	
	
	
	
}
