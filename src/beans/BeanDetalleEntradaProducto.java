package beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;

import model.DetalleEntradaProducto;
import model.EntradaProducto;
import model.Producto;
import model.empleado;
import services.DetalleEntradaProductoService;
import services.EntradaProductoService;
import services.ProductoService;
import services.empleadoService;

@ManagedBean(name="BeanDetalleEntradaProducto")

@ViewScoped
public class BeanDetalleEntradaProducto
{
	private DetalleEntradaProducto detEntPro = new DetalleEntradaProducto();
	private EntradaProducto entPro = new EntradaProducto();
	private Producto prod = new Producto();
	private empleado emp = new empleado();
	private empleadoService emplService = new empleadoService();
	private DetalleEntradaProductoService detEntProService = new DetalleEntradaProductoService();
	private EntradaProductoService entProService = new EntradaProductoService();
	private ProductoService proService = new ProductoService();
	private List<Producto> listaProducto = new ArrayList<>();
	private List<empleado> listaEmpleadoo = new ArrayList<>();
	private List<DetalleEntradaProducto> listaDetEntPro = new ArrayList<>();
	private List<DetalleEntradaProducto> listDetEntProSelec = new ArrayList<>();
	private List<EntradaProducto> listEnt = new ArrayList<>();

	
	public BeanDetalleEntradaProducto() throws Exception
	{
		listarEntradProd();
		listarEmpleado();
		listarProducto();
		
	}

	
	public void listarEmpleado() throws Exception
	{
		listaEmpleadoo = emplService.listar();
	}
	
	public void listarEntradProd() throws Exception
	{
		this.listEnt = entProService.listar();
	}
	
	public void listarProducto() throws Exception 
	{
		listaProducto = proService.ListarSinDetalle();
	}

	public void RegistrarEntrada() throws Exception
	{
		if (listaDetEntPro.size()>0 && emp!= null) 
		{
			entPro.setTblDetEntPro(listaDetEntPro);
			
			entPro.setFechaEntrada(new Date());
			
			emp.setTblEntPro(new ArrayList<EntradaProducto>(Arrays.asList(entPro)));
			entPro.setTblEmpleado(emp);
			
			if (entProService.registrar(entPro))
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"OK Entrada Registrada correctamente",""));
				RequestContext.getCurrentInstance().execute("PF('dialogoRegistrar').hide()");
				RequestContext.getCurrentInstance().update("tablaSal");
				//esto recorre la lista de detalles y por cada elemento actualiza el stock
				for (DetalleEntradaProducto detalleEntradaProducto : listaDetEntPro) 
				{
					//aqui se asigna el nuevo stock a cada uno de los productos en la lista de detalles
					detalleEntradaProducto.getTblProducto().setStock(detalleEntradaProducto.getTblProducto().getStock().add(new BigDecimal( detalleEntradaProducto.getCantidad())));
					//luego se procede a actualizar cada 1
					proService.ActualizarSinDetalle(detalleEntradaProducto.getTblProducto());
				}
			} 
			else
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Entrada No Registrada",""));
			}
			
			prod = new Producto();
			detEntPro = new DetalleEntradaProducto();
			entPro = new EntradaProducto();
			emp = new empleado();
			listaDetEntPro = new ArrayList<>();
			listarEntradProd();
		} 
		else 
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe rellenar todos los campos",""));
		}
		
		
		
	}
	
	public void addEntPro() 
	{
		detEntPro.setTblEntPro(entPro);
		
		if(listaDetEntPro.isEmpty())
		{
			if(detEntPro.getCantidad()==0)
			{
				FacesContext.getCurrentInstance().addMessage
				(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad > a 0",""));
			}
			else 
			{
				listaDetEntPro.add(detEntPro);	
			}
			
		}
		else
		{
			boolean existe=false;
			int index=0;
			for (int i = 0; i < listaDetEntPro.size(); i++) 
			{
				if(detEntPro.getTblProducto().getCodigoBarra().equals(listaDetEntPro.get(i).getTblProducto().getCodigoBarra()))	
				{
					existe=true;
					index=i;
				}
			}
			if(existe)
			{
				if(detEntPro.getCantidad()==0)
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad > a 0",""));
				
				}
				else
				{
					listaDetEntPro.get(index).setCantidad(listaDetEntPro.get(index).getCantidad() + detEntPro.getCantidad());
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"El producto ya existe, Se agregaron " + detEntPro.getCantidad() + "Unidades", ""));
					
				}
				{
					
				}
			}
			else 
			{
				if(detEntPro.getCantidad()==0)
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Debe especificar una cantidad > a 0",""));
				}
				else
				{
					listaDetEntPro.add(detEntPro);
				}
				
			}
		}
		
		
		detEntPro = new DetalleEntradaProducto();
	}
	
	public void delet(DetalleEntradaProducto en)
	{
		System.out.println(listaDetEntPro.size());
		listaDetEntPro.remove(en);
		System.out.println(listaDetEntPro.size());
	}
	
	public void select(List<DetalleEntradaProducto> en)
	{
		listDetEntProSelec = en;
	}
	
	public void hola()
	{
		System.out.println("Hola");
	}

	public DetalleEntradaProducto getDetEntPro() {
		return detEntPro;
	}


	public void setDetEntPro(DetalleEntradaProducto detEntPro) {
		this.detEntPro = detEntPro;
	}


	public EntradaProducto getEntPro() {
		return entPro;
	}


	public void setEntPro(EntradaProducto entPro) {
		this.entPro = entPro;
	}


	public Producto getProd() {
		return prod;
	}


	public void setProd(Producto prod) {
		this.prod = prod;
	}


	public empleado getEmp() {
		return emp;
	}


	public void setEmp(empleado emp) {
		this.emp = emp;
	}


	public empleadoService getEmplService() {
		return emplService;
	}


	public void setEmplService(empleadoService emplService) {
		this.emplService = emplService;
	}


	public DetalleEntradaProductoService getDetEntProService() {
		return detEntProService;
	}


	public void setDetEntProService(DetalleEntradaProductoService detEntProService) {
		this.detEntProService = detEntProService;
	}


	public EntradaProductoService getEntProService() {
		return entProService;
	}


	public void setEntProService(EntradaProductoService entProService) {
		this.entProService = entProService;
	}


	public List<empleado> getListaEmpleadoo() {
		return listaEmpleadoo;
	}


	public void setListaEmpleadoo(List<empleado> listaEmpleadoo) {
		this.listaEmpleadoo = listaEmpleadoo;
	}


	public List<DetalleEntradaProducto> getListaDetEntPro() {
		return listaDetEntPro;
	}


	public void setListaDetEntPro(List<DetalleEntradaProducto> listaDetEntPro) {
		this.listaDetEntPro = listaDetEntPro;
	}


	public List<DetalleEntradaProducto> getListDetEntProSelec() {
		return listDetEntProSelec;
	}


	public void setListDetEntProSelec(List<DetalleEntradaProducto> listDetEntProSelec) {
		this.listDetEntProSelec = listDetEntProSelec;
	}


	public List<EntradaProducto> getListEnt() {
		return listEnt;
	}


	public void setListEnt(List<EntradaProducto> listEnt) {
		this.listEnt = listEnt;
	}


	public ProductoService getProService() {
		return proService;
	}


	public void setProService(ProductoService proService) {
		this.proService = proService;
	}


	public List<Producto> getListaProducto() {
		return listaProducto;
	}


	public void setListaProducto(List<Producto> listaProducto) {
		this.listaProducto = listaProducto;
	}
	
	
	

	
}
