package beans;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.event.ActionEvent;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;

import java.io.File;

import model.Categoria;
import model.DetalleProducto;
import model.Producto;
import model.ProductoProveedor;
import model.Proveedor;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import services.CategoriaService;
import services.DetalleEntradaProductoService;
import services.MetodosGenericosJPQLService;
import services.ProductoService;
import services.ProveedorService;
import util.FinalVariables;
import util.RepSalidasYear;
import util.conexion;
import util.misc;

@ManagedBean(name = "BeanProducto")
@ViewScoped

public class BeanProducto {
	private Producto pro = new Producto();
	private Categoria cat = new Categoria();
	private Proveedor prov = new Proveedor();
	private Producto prodsel = new Producto();
	private List<Proveedor> provs;
	private List<Proveedor> asd = new ArrayList<>();
	private Producto prodselregprov = new Producto();
	private List<Proveedor> provadd = new ArrayList<>();
	private ProveedorService provService = new ProveedorService();
	private DetalleProducto det = new DetalleProducto();
	private DetalleProducto detact = new DetalleProducto();
	List<Producto> prods;
	private BigDecimal NPrecio;
	private ProductoService proservice = new ProductoService();
	private List<Proveedor> SelectedProviders;


	
	public BeanProducto() throws Exception {
		ListarProducto();
		
	}

	public void SeleccionarProveedores(List<Proveedor> lst)
	{
		this.SelectedProviders=lst;
		System.out.println(SelectedProviders.size()+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxvxxx");
	}
	
	
	public void Eliminar(Producto o) throws Exception
	{
		if(proservice.Eliminar(o))
		{
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO,
							"OK:Producto eliminado Correctamente", null));
			
			ListarProducto();
			
			RequestContext.getCurrentInstance().update("frm");
			
		}
		else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR:No se pudo eliminar el producto", null));
		}
		
	}
	
	
	public void agregarProducto() {
		pro.setFechaRegistro(new Date());
		det.setEstado(1);
		pro.setTblDetPro(new ArrayList<DetalleProducto>(Arrays.asList(det)));
		det.setTblProducto(pro);
		pro.setTblCategoria(cat);
		cat.setTblProducto(new ArrayList<Producto>(Arrays.asList(pro)));
		
		pro.setProveedores(provs);
		
			try {
				proservice.agregarProducto(pro);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO,
								"OK:Producto registrado exitosamente!", null));
				RequestContext.getCurrentInstance().execute("PF('rprod').hide();");
				RequestContext.getCurrentInstance().update("frm");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				String ex=e.getCause().getCause().getCause().toString();
				if(ex.contains(FinalVariables.DUPLICATED))
				{

					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR:Ya existe un producto con este codigo de barra", ""));
				}
				else

					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"ERROR:No se pudo registrar el Producto", null));
			}
		
		

		provs = new ArrayList<>();
		pro = new Producto();
		det = new DetalleProducto();
		cat = new Categoria();
		try {
			ListarProducto();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	////////////////
	
	public List<Producto> BU(String o) throws Exception
	{
		return proservice.Listar(o);
	}
	
	
	public List<Proveedor> BU2(String o) throws Exception
	{
		return provService.Listar(o);
	}

	public void ActualizarProducto() throws Exception {

		detact.getTblProducto().setTblCategoria(cat);

		if (proservice.ActualizarProducto(detact, NPrecio)) {

			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"EXITO!!!! El producto fue actualizado correctamente",
					null));
			System.err.println("Se actualizo el Producto correctamente");
			RequestContext.getCurrentInstance()
					.execute("PF('UpdateDialog').hide();");
			RequestContext.getCurrentInstance().update("frm");

		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Error!!!! No se pudo actualizar el producto",
							null));
			System.err.println("No se pudo actualizar el Producto");

		}

		pro = new Producto();
		detact = new DetalleProducto();
		cat = new Categoria();
		NPrecio = null;
		ListarProducto();
	}

	///////////////////////////////////
	public void SeleccionarDetalleProducto(DetalleProducto det)
			throws Exception {

		this.detact = det;
		System.out.println(
				"BeanProducto.SeleccionarDetalleProducto()xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

	}

	public void SeleccionarProductoRegProv(Producto obj) {

		this.prodselregprov = obj;

	}

	public void SeleccionarProducto(Producto obj) {

		this.prodsel = obj;

	}

	public void ListarProducto() throws Exception {
		prods = proservice.Listar();

	}

	public void AgregarProveedor(Proveedor obj) throws Exception {

		ProductoProveedor productoProveedor = new ProductoProveedor();
		productoProveedor.setIdProducto(prodsel.getIdProducto());
		productoProveedor.setIdProveedor(obj.getIdProveedor());

		if (proservice.agregarIntermedia(productoProveedor)) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO,
							"OK: Proveedor asignado exitosamente!", null));
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"ERROR:No se pudo asignar el proveedor", null));
		}

		prodsel = new Producto();
	}

	public void ListarProveedoresFiltro() throws Exception {
		this.asd = proservice.mlpf();
		System.out.println(asd.size()
				+ "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxvxxxxxxxxxxxxxxxx");
		System.out.println(asd.get(0).getRazonSocial());
	}

	// public void CleanMessages()
	// {
	// misc.ClearMessages();
	//
	// RequestContext.getCurrentInstance().execute("PF('rprod').show()");
	//
	//
	// }
	//

	
	
	
//	private void executeNative(final String query) {
//		conexion cn = new conexion();
//	    Session session = cn.em.unwrap(Session.class);
//	    session.doWork(new Work() {
//			
//			@Override
//			public void execute(Connection connection) throws SQLException {
//				  Statement s = null;
//		            try {
//		                s = connection.createStatement();
//		                s.executeUpdate(query);
//		            } 
//		            
//		            finally {
//		                if (s != null) {
//		                    s.close();
//		                }
//		            }
//				
//			}
//		});
//
//
//	    	
//	}
	
	

	
	public void exportarPDF(ActionEvent e, int cantidad) {
		try {
			MetodosGenericosJPQLService mgs = new MetodosGenericosJPQLService();
			Map<String,Object> parametros = new HashMap<String, Object>();

			File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/paginas/dialogs/producto/reportes/a.jasper"));
//			String query ="select nombre,marca,CAST(stock AS CHAR) as stock from producto where stock<"+cantidad;
//
//			List<Producto> replst=new ArrayList<Producto>();
//			
//			
//			 List lst =mgs.ListarGenerico(query);
//			for (Iterator<Object> it = lst.iterator(); it.hasNext();) {
//	            Object[] object = (Object[]) it.next();
//	            String nombre =  (String)object[0];
//	            String marca = (String) object[1];
//	            String stock = (String) object[2];
//	            System.err.println(nombre+" "+marca+ " " +stock);
//	            replst.add(new Producto(nombre,marca, new BigDecimal(stock)));
//			}
			
			List<Producto> replst=proservice.ListarCondicionado("where p.stock<"+cantidad);
			System.err.println(replst.size()+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
			System.err.println(jasper.getAbsolutePath()+"");
			JasperPrint jasperprint = JasperFillManager.fillReport(jasper.getPath(),null,new JRBeanCollectionDataSource(replst));
			
			HttpServletResponse response = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
			response.addHeader("Content-disposition", "attachment; filename=StockBajo.pdf");
			ServletOutputStream stream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperprint,stream);
			
			stream.flush();
			stream.close();
			FacesContext.getCurrentInstance().responseComplete();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	public void SalidasYear(ActionEvent e, String y) {
		try {

			File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/paginas/dialogs/salidaproducto/reportes/salidasyear.jasper"));
			
			List<RepSalidasYear> replst=proservice.SalidasAnio(y);
			System.err.println(replst.size()+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
			
			JasperPrint jasperprint = JasperFillManager.fillReport(jasper.getPath(),null,new JRBeanCollectionDataSource(replst));
			
			HttpServletResponse response = (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
			response.addHeader("Content-disposition", "attachment; filename=StockBajo.pdf");
			ServletOutputStream stream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperprint,stream);
			
			stream.flush();
			stream.close();
			FacesContext.getCurrentInstance().responseComplete();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}




	public Producto getPro() {
		return pro;
	}




	public void setPro(Producto pro) {
		this.pro = pro;
	}




	public Categoria getCat() {
		return cat;
	}




	public void setCat(Categoria cat) {
		this.cat = cat;
	}




	public Proveedor getProv() {
		return prov;
	}




	public void setProv(Proveedor prov) {
		this.prov = prov;
	}




	public Producto getProdsel() {
		return prodsel;
	}




	public void setProdsel(Producto prodsel) {
		this.prodsel = prodsel;
	}




	public List<Proveedor> getAsd() {
		return asd;
	}




	public void setAsd(List<Proveedor> asd) {
		this.asd = asd;
	}




	public Producto getProdselregprov() {
		return prodselregprov;
	}




	public void setProdselregprov(Producto prodselregprov) {
		this.prodselregprov = prodselregprov;
	}




	public List<Proveedor> getProvadd() {
		return provadd;
	}




	public void setProvadd(List<Proveedor> provadd) {
		this.provadd = provadd;
	}




	public List<Proveedor> getSelectedProviders() {
		return SelectedProviders;
	}




	public void setSelectedProviders(List<Proveedor> selectedProviders) {
		SelectedProviders = selectedProviders;
	}




	public DetalleProducto getDet() {
		return det;
	}




	public void setDet(DetalleProducto det) {
		this.det = det;
	}




	public DetalleProducto getDetact() {
		return detact;
	}




	public void setDetact(DetalleProducto detact) {
		this.detact = detact;
	}




	public List<Producto> getProds() {
		return prods;
	}




	public void setProds(List<Producto> prods) {
		this.prods = prods;
	}




	public BigDecimal getNPrecio() {
		return NPrecio;
	}




	public void setNPrecio(BigDecimal nPrecio) {
		NPrecio = nPrecio;
	}




	public List<Proveedor> getProvs() {
		return provs;
	}




	public void setProvs(List<Proveedor> provs) {
		this.provs = provs;
	}


	

}
