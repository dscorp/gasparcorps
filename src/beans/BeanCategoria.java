package beans;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import model.Categoria;
import services.CategoriaService;

@ManagedBean(name="BeanCategoria")
@ViewScoped
public class BeanCategoria 
{

	private Categoria cat=new Categoria();
	private CategoriaService catservice = new CategoriaService();

	
	
	
	public void agregar() throws Exception
	{
		
		if (catservice.Registrar(cat))
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"OK: Categoria Registrada Correctamente",""));
		}
		else
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error: La categoria no pudo ser registrada",""));
		}
		
		cat= new Categoria();
	}
	
	public List<Categoria> listar() throws Exception
	{
		return catservice.listar();
	}

	public Categoria getCat() {
		return cat;
	}

	public void setCat(Categoria cat) {
		this.cat = cat;
	}

	public CategoriaService getCatservice() {
		return catservice;
	}

	public void setCatservice(CategoriaService catservice) {
		this.catservice = catservice;
	}
	
	
	
}
