package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;

import model.Usuario;
import model.empleado;
import services.UsuarioService;
import services.empleadoService;
import util.SessionUtils;

@ManagedBean(name="BeanUsuario")
@ViewScoped

public class BeanUsuario 
{
	private String f="Administrador" , s="Almacenero";
//	private String s="Almacenero";
	//////////////////////
	private Usuario usu = new Usuario();
	private Usuario logusu= new Usuario();
	private Usuario usuact = new Usuario();
	private empleado selectedemp = new empleado();
	private empleado emp = new empleado();
	private UsuarioService ususerv = new UsuarioService();
	private List<Usuario> allusu = new ArrayList<>();
	private List<empleado> allemp = new ArrayList<>();
	private empleadoService empserv = new empleadoService();
	public BeanUsuario() throws Exception

	{
//		ListarEmpeados();
		listarEmpleados2();
		
	}
	
	
	public String IniciarSession() throws Exception {
		boolean  valido = ususerv.IniciarSession(logusu);
		
		if (valido) {
			HttpSession session = SessionUtils.getSession();
			session.setAttribute("username", logusu.getUsuario());
			return "admin";
		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_WARN,
							"Credenciales invalidas",
							"Por favor ingrese un usuario y contraseņa validos"));
			return "Login";
		}
	}
	
	public String CerrarSession() {
		HttpSession session = SessionUtils.getSession();
		session.invalidate();
		return "Login";
	}
	
		public void agregarUsuario() throws Exception
		{
			try 
			{
				usu.setTblEmpleado(selectedemp);
				selectedemp.setTblUsuario(usu);

				if(ususerv.agregarUsuario(usu)) 
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"OK: Usuario registrado exitosamente!",null));
					
					
					RequestContext.getCurrentInstance().execute("PF('rprod').hide();");
					RequestContext.getCurrentInstance().update("frm");
					
				}
				else
				{
					FacesContext.getCurrentInstance().addMessage
					(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR:No se pudo registrar el Usuario",null));
				}
						
				usu= new Usuario();
				selectedemp= new empleado();
				ListarUsuario();
				
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR!!!! No se pudo listar los usuarios ",null));
				
			}
			
			
			
			
		}

		///////////////////////////////////
		public void EmpeadoSeleccionado(empleado obj) throws Exception
		{
			System.out.println(obj.getTblpersona().getApellidoPaterno()+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
			this.selectedemp=obj;
			System.out.println("BeanUsuario.EmpeadoSeleccionado()");
		}

		
		public void UsuarioSeleccionado(Usuario obj) throws Exception
		{
			obj.setContrasena("");
			this.usuact=obj;
			
		}
		
		public void ListarUsuario() throws Exception
		{
			try {
			
System.out.println("BeanUsuario.ListarUsuario()");
				allusu=ususerv.Listar();
			
				System.out.println("BeanUsuario.ListarUsuario()");
			} catch (Exception e) {
				e.printStackTrace();
				
			}
			}
	
	
		public void ListarEmpeados() throws Exception
		{
			allemp= empserv.ListarPorCargo(f);
			System.out.println(allemp.size() +"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		}
		
		public void listarEmpleados2() throws Exception
		{
			allemp = empserv.listarPorCargo2(f, s);
			System.out.println(allemp.size() +"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		}
		
		
	public void ActualizarUsuario() throws Exception
	{
		

		System.out.println("BeanUsuario.ActualizarUsuario()");
		if (ususerv.ActualizarUsuario(usuact)) 
			{

				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"EXITO!!!! El Usuario fue actualizado correctamente",null));
				System.err.println("Se actualizo el Usuario correctamente");
				
				RequestContext.getCurrentInstance().execute("PF('rprod').hide();");
				RequestContext.getCurrentInstance().update("frm");
				
			} 
		else 
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error!!!! No se pudo actualizar el Usuario",null));
				System.err.println("No se pudo actualizar el Usuario");
				
			}
		usuact= new Usuario();
		emp= new empleado();
		ListarUsuario();
	}

	public Usuario getUsu() {
		return usu;
	}

	public void setUsu(Usuario usu) {
		this.usu = usu;
	}

	public Usuario getUsuact() {
		return usuact;
	}

	public void setUsuact(Usuario usuact) {
		this.usuact = usuact;
	}

	public empleado getEmp() {
		return emp;
	}

	public void setEmp(empleado emp) {
		this.emp = emp;
	}

	public UsuarioService getUsuserv() {
		return ususerv;
	}

	public void setUsuserv(UsuarioService ususerv) {
		this.ususerv = ususerv;
	}

	public List<Usuario> getAllusu() {
		return allusu;
	}

	public void setAllusu(List<Usuario> allusu) {
		this.allusu = allusu;
	}

	public List<empleado> getAllemp() {
		return allemp;
	}

	public void setAllemp(List<empleado> allemp) {
		this.allemp = allemp;
	}

	public empleado getSelectedemp() {
		return selectedemp;
	}

	public void setSelectedemp(empleado selectedemp) {
		this.selectedemp = selectedemp;
	}

	public String getF() {
		return f;
	}

	public void setF(String f) {
		this.f = f;
	}

	public empleadoService getEmpserv() {
		return empserv;
	}

	public void setEmpserv(empleadoService empserv) {
		this.empserv = empserv;
	}


	public Usuario getLogusu() {
		return logusu;
	}


	public void setLogusu(Usuario logusu) {
		this.logusu = logusu;
	}


	public String getS() {
		return s;
	}


	public void setS(String s) {
		this.s = s;
	}

	
	
	
}
