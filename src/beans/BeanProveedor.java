package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import model.InfoContacto;
import model.Proveedor;

import services.InfoContactoService;
import services.ProveedorService;
import util.FinalVariables;
import util.misc;

@ManagedBean(name="BeanProveedor")
@ViewScoped
public class BeanProveedor 
{
	
	private InfoContacto inf=new InfoContacto();
	private ProveedorService proService = new ProveedorService();
	private InfoContactoService infService = new InfoContactoService();
	private Proveedor proveedor= new Proveedor();
	private Proveedor proselected = new Proveedor();
	private List<Proveedor> allprov;
	private InfoContacto infselcected = new InfoContacto();
	
	

	public BeanProveedor() throws Exception
	{
		Listar();
	}
	
	
	public void SeleccionarInfoContato(InfoContacto obj)
	{
		this.infselcected=obj;
	}
	
	
	public void Registrar() 
	{
		proveedor.setFechaRegistro(new Date());
		
		inf.setTblProveedor(proveedor);
		proveedor.setTblInfoContacto(inf);
		
		
		
		try {
			infService.RegistrarInfoContacto(inf);
			
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "OK: Provedor registrado", ""));
			
			RequestContext.getCurrentInstance().execute("PF('dialogoRegistrar').hide()");
			RequestContext.getCurrentInstance().update("formprov");
			
		} catch (Exception e) {
			
			
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR:No se pudo registrar el proveedor ", ""));
		
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, e.getCause().getCause().getCause().toString(), ""));
		
			
			
	
			
		}
	
			
		
	
	
		
		inf = new InfoContacto();
		proveedor = new Proveedor();
		try {
			Listar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void Listar() throws Exception
	{
		this.allprov=proService.listar();
		
	}
		
	public void seleccionarProv(Proveedor obj) throws Exception
	{

		this.proselected = obj;
		
		
	}
	
	public void actualizar() throws Exception
	{	
		
		if (proService.actualizar(proselected)) 
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"OK:Proveedor Actualizado",""));
			
			RequestContext.getCurrentInstance().execute("PF('dlgact').hide()");
			RequestContext.getCurrentInstance().update("formprov");

		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage
			(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR: Proveedor No Actualizado",""));
		}
		
		proveedor = new Proveedor();
		inf = new InfoContacto();
		Listar();
		
	}


	public InfoContacto getInf() {
		return inf;
	}

	public void setInf(InfoContacto inf) {
		this.inf = inf;
	}

	public ProveedorService getProService() {
		return proService;
	}

	public void setProService(ProveedorService proService) {
		this.proService = proService;
	}

	public InfoContactoService getInfService() {
		return infService;
	}

	public void setInfService(InfoContactoService infService) {
		this.infService = infService;
	}



	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Proveedor getProselected() {
		return proselected;
	}

	public void setProselected(Proveedor proselected) {
		this.proselected = proselected;
	}

	public List<Proveedor> getAllprov() {
		return allprov;
	}

	public void setAllprov(List<Proveedor> allprov) {
		this.allprov = allprov;
	}

	public InfoContacto getInfselcected() {
		return infselcected;
	}

	public void setInfselcected(InfoContacto infselcected) {
		this.infselcected = infselcected;
	}




}
