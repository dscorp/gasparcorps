package DAOlocal;

import java.util.List;

import model.Usuario;

public interface UsuarioDaoLocal 
{
	public abstract boolean agregarUsuario(Usuario obj) throws Exception;
	public abstract  List<Usuario> Listar() throws Exception;
	public abstract boolean ActualizarUsuario(Usuario obj)  throws Exception;
	public abstract boolean IniciarSession(Usuario obj) throws Exception;
}
