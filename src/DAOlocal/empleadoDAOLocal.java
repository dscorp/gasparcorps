package DAOlocal;

import java.util.List;

import model.empleado;


public interface empleadoDAOLocal {

	public boolean Registrar(empleado empleado) throws Exception;
	public List<empleado> listar() throws Exception;
	public List<empleado> ListarPorCargo(String o) throws Exception;
	public abstract boolean actualizar(empleado empleado) throws  Exception;
	
	public List<empleado> listarPorCargo2(String m, String n) throws Exception;
	
}
