package DAOlocal;

import java.util.List;

import model.EntradaProducto;
import model.empleado;

public interface EntradaProductoDAOLocal 
{
	public abstract boolean registrar(EntradaProducto entradaProducto) throws Exception;
	public List<empleado> listarE() throws Exception;
	public List<EntradaProducto> listar() throws Exception;
}
