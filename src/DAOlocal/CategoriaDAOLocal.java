package DAOlocal;

import java.util.List;

import model.Categoria;

public interface CategoriaDAOLocal {

	public abstract boolean Registrar(Categoria categoria) throws Exception;
	public List<Categoria> listar() throws Exception;
	public abstract boolean Actualizar(Categoria categoria) throws Exception;
	

	
}
