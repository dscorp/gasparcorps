package DAOlocal;

import java.util.List;
import model.DetalleProducto;
import model.InfoContacto;
import model.Producto;
import model.Proveedor;

public interface InfoContatoDaoLocal 
{
	public abstract void RegistrarInfoContacto(InfoContacto obj) throws Exception ;
	public abstract boolean ActualizarInfoContacto(InfoContacto obj) throws Exception; 
}
