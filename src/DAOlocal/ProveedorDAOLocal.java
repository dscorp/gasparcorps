package DAOlocal;

import java.util.List;

import model.Producto;
import model.Proveedor;

public interface ProveedorDAOLocal 
{
	public abstract boolean registrar(Proveedor proveedor) throws Exception;
	public abstract List<Proveedor> listar() throws Exception;
	public abstract  List<Proveedor> Listar(String o) throws Exception;
	public abstract boolean actualizar(Proveedor proveedor) throws Exception;
	public abstract Proveedor Buscar(String id) throws Exception;
}
