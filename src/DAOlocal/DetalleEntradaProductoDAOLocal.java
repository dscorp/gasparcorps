package DAOlocal;

import java.util.List;

import model.DetalleEntradaProducto;
import model.Producto;
import model.empleado;

public interface DetalleEntradaProductoDAOLocal 
{
	public List<Producto> listarP() throws Exception;
	public List<DetalleEntradaProducto> listar() throws Exception;
	public abstract boolean actualizar(DetalleEntradaProducto detEntPro) throws Exception;
}
