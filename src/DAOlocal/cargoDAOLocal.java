package DAOlocal;

import java.util.List;

import model.Cargo;



public interface cargoDAOLocal
{
	public abstract boolean registrar(Cargo cargo) throws Exception;
	public List<Cargo> listar() throws Exception;
	public abstract boolean actualizar(Cargo cargo)throws Exception;
}
