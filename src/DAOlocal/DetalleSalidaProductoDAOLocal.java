package DAOlocal;

import java.util.List;

import model.DetalleSalidaProducto;
import model.Producto;
import model.empleado;

public interface DetalleSalidaProductoDAOLocal 
{
	public List<Producto> listarP() throws Exception;
	public List<DetalleSalidaProducto> listar() throws Exception;
}
