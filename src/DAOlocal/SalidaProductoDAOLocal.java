package DAOlocal;

import java.util.List;

import model.EntradaProducto;
import model.SalidaProducto;
import model.empleado;

public interface SalidaProductoDAOLocal 
{
	public abstract boolean registrar(SalidaProducto salidaProducto) throws Exception;
	public List<empleado> listarE() throws Exception;
	public List<SalidaProducto> listar() throws Exception;
}
