package DAOlocal;

import java.math.BigDecimal;
import java.util.List;



import model.DetalleProducto;
import model.Producto;
import model.ProductoProveedor;
import model.Proveedor;
import util.RepSalidasYear;

public interface ProductoDaoLocal 
{
	public abstract void agregarProducto(Producto obj) throws Exception;
	public abstract  List<Producto> Listar() throws Exception;
	public abstract  List<Producto> Listar(String o) throws Exception;
//	public abstract List<Categoria> listarCBXCategoria() throws Exception;
	public abstract boolean ActualizarProducto(DetalleProducto dp , BigDecimal NPrecio)  throws Exception;
	public abstract boolean agregarIntermedia(ProductoProveedor obj) throws Exception;
	public abstract List<Proveedor> mlpf() throws Exception;
	public abstract List<Producto> ListarSinDetalle() throws Exception;
	public abstract Producto Buscar(String id) throws Exception;
	public abstract boolean Eliminar(Producto o) throws Exception; 
	public abstract List<Producto>  ListarCondicionado(String c) throws Exception;
	public abstract List<RepSalidasYear> SalidasAnio(String y) throws Exception;
	public abstract void ActualizarSinDetalle(Producto obj) throws Exception;
}
