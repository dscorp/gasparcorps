package services;

import java.math.BigDecimal;
import java.util.List;

import DAOImp.ProductoDaoImpl;
import model.Categoria;
import model.DetalleProducto;
import model.Producto;
import model.ProductoProveedor;
import model.Proveedor;
import util.RepSalidasYear;

public class ProductoService extends ProductoDaoImpl
{





	@Override
	public void agregarProducto(Producto obj) throws Exception {
		// TODO Auto-generated method stub
		super.agregarProducto(obj);
	}

	@Override
	public List<Producto> Listar() throws Exception {
		// TODO Auto-generated method stub
		return super.Listar();
	}

	@Override
	public boolean ActualizarProducto(DetalleProducto dp, BigDecimal NPrecio) throws Exception {
		// TODO Auto-generated method stub
		return super.ActualizarProducto(dp, NPrecio);
	}

	@Override
	public boolean agregarIntermedia(ProductoProveedor obj) throws Exception {
		// TODO Auto-generated method stub
		return super.agregarIntermedia(obj);
	}

	@Override
	public List<Proveedor> mlpf() throws Exception {
		// TODO Auto-generated method stub
		return super.mlpf();
	}

	@Override
	public List<Producto> ListarSinDetalle() throws Exception {
		// TODO Auto-generated method stub
		return super.ListarSinDetalle();
	}

	@Override
	public Producto Buscar(String id) throws Exception {
		// TODO Auto-generated method stub
		return super.Buscar(id);
	}

	@Override
	public List<Producto> Listar(String o) throws Exception {
		// TODO Auto-generated method stub
		return super.Listar(o);
	}

	@Override
	public boolean Eliminar(Producto o) throws Exception {
		// TODO Auto-generated method stub
		return super.Eliminar(o);
	}

	@Override
	public List<Producto> ListarCondicionado(String c) throws Exception {
		// TODO Auto-generated method stub
		return super.ListarCondicionado(c);
	}

	@Override
	public List<RepSalidasYear> SalidasAnio(String y) throws Exception {
		// TODO Auto-generated method stub
		return super.SalidasAnio(y);
	}

	@Override
	public void ActualizarSinDetalle(Producto obj) throws Exception {
		// TODO Auto-generated method stub
		super.ActualizarSinDetalle(obj);
	}

	
	
}
