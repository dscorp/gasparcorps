package model;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="detalleproducto")
public class DetalleProducto 
{
	@Id
	@GeneratedValue
	private int idDetalleProducto;
	
	@NotNull(message = "*El campo Precio es requerido")
//	@Digits(integer=3, fraction = 2,message="El stock puede contener 3 enteros y 2 decimales como maximo")
	private BigDecimal precio=null;
	
	
	private int estado;
	
	@ManyToOne
	@JoinColumn(name="idProducto")
	private Producto tblProducto;

	public int getIdDetalleProducto() {
		return idDetalleProducto;
	}

	public void setIdDetalleProducto(int idDetalleProducto) {
		this.idDetalleProducto = idDetalleProducto;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Producto getTblProducto() {
		return tblProducto;
	}

	public void setTblProducto(Producto tblProducto) {
		this.tblProducto = tblProducto;
	}




	
	
	
	
	
}
