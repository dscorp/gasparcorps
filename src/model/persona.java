package model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="persona")
public class persona 
{
	@Id
	@NotNull(message="*EL campo DNI es requerido")
	@Size(min =8 , max =8, message = "El DNI debe ser de 8 digitos")
	private String dniPersona;
	
	@NotEmpty(message= "*El campo nombre es requerido")
	@Size(max = 30, message = "El campo nombre solo admite 30 caracteres")
	@Pattern(regexp = "^[a-zA-Z ]*$", message = "El campo nombre solo permite letras")
	private String nombre;
	
	@NotEmpty(message= "*El campo apellido paterno es requerido")
	@Size(max = 15, message = "El campo apellido paterno solo admite 15 caracteres")
	@Pattern(regexp = "^[a-zA-Z ]*$", message = "El campo apellido paterno solo permite letras")
	private String apellidoPaterno;
	
	@NotEmpty(message= "*El campo apellido materno es requerido")
	@Size(max = 15, message = "El campo apellido materno solo admite 15 caracteres")
	@Pattern(regexp = "^[a-zA-Z ]*$", message = "El campo apellido materno solo permite letras")
	private String apellidoMaterno;
	
	@NotNull(message="El campo FechaNacimiento es requerido")
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;
	
	@OneToOne(mappedBy = "tblpersona",cascade=CascadeType.ALL)
    private empleado tblempleado;
	
	
	
	
	
	

	
	
	@OneToOne(cascade= {CascadeType.MERGE})
	@JoinColumn(name="idInfoContacto")
	private InfoContacto tblInfoContacto;




	public String getDniPersona() {
		return dniPersona;
	}




	public void setDniPersona(String dniPersona) {
		this.dniPersona = dniPersona;
	}




	public String getNombre() {
		return nombre;
	}




	public void setNombre(String nombre) {
		this.nombre = nombre;
	}




	public String getApellidoPaterno() {
		return apellidoPaterno;
	}




	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}




	public String getApellidoMaterno() {
		return apellidoMaterno;
	}




	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}




	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}




	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}




//	public empleado getTblempleado() {
//		return tblempleado;
//	}
//
//
//
//
//	public void setTblempleado(empleado tblempleado) {
//		this.tblempleado = tblempleado;
//	}




	public InfoContacto getTblInfoContacto() {
		return tblInfoContacto;
	}




	public void setTblInfoContacto(InfoContacto tblInfoContacto) {
		this.tblInfoContacto = tblInfoContacto;
	}




	public empleado getTblempleado() {
		return tblempleado;
	}




	public void setTblempleado(empleado tblempleado) {
		this.tblempleado = tblempleado;
	}

	
	
	
	


}
