package model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="detalleSalidaProducto")
public class DetalleSalidaProducto 
{
	@Id
	@GeneratedValue
	private int idDetalleSalidaProducto;
	
	@NotNull(message = "*El campo cantidad es requerido")
	@Digits(integer=3, fraction = 2,message="La cantidad puede contener 3 enteros y 2 decimales como maximo")
	private double cantidad;
	
	@ManyToOne()
	@JoinColumn(name="idProducto")
	private Producto tblProducto;
	
	@ManyToOne(cascade=CascadeType.REMOVE)
	@JoinColumn(name="idSalidaProducto")
	private SalidaProducto tblSalPro;

	public int getIdDetalleSalidaProducto() {
		return idDetalleSalidaProducto;
	}

	public void setIdDetalleSalidaProducto(int idDetalleSalidaProducto) {
		this.idDetalleSalidaProducto = idDetalleSalidaProducto;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public Producto getTblProducto() {
		return tblProducto;
	}

	public void setTblProducto(Producto tblProducto) {
		this.tblProducto = tblProducto;
	}

	public SalidaProducto getTblSalPro() {
		return tblSalPro;
	}

	public void setTblSalPro(SalidaProducto tblSalPro) {
		this.tblSalPro = tblSalPro;
	}
	
	
}
