package model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="cargo")
public class Cargo 
{
	@Id
	@GeneratedValue
	private int idCargo;
	@NotEmpty(message= "*El campo de cargo es requerido")
	@Size(max = 25, message = "El campo cargo solo admite 25 letras")
	@Pattern(regexp = "^[a-zA-Z ]*$", message = "El campo cargo solo permite letras")
	private String descripcion;
	
	@OneToMany(mappedBy="tblcargo")
	private List<empleado> tblempleado;

	public int getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(int idCargo) {
		this.idCargo = idCargo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<empleado> getTblempleado() {
		return tblempleado;
	}

	public void setTblempleado(List<empleado> tblempleado) {
		this.tblempleado = tblempleado;
	}
	
	
}

