package model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import util.CifradoUtils;

@Entity
@Table(name = "usuario")
public class Usuario {

	@Id
	@GeneratedValue
	private int idUsuario;

	@NotEmpty(message= "*El campo Usuario es requerido")
	@Size(max = 15, message = "El campo Usuario solo admite 15 caracteres")

	private String usuario;

	
	@NotEmpty(message= "*El campo Password es requerido")
//	@Size( max = 16, message = "El campo password debe ser entre 6 y 16 caracteres")
	private String contrasena;


  @OneToOne()
  @JoinColumn(name = "idEmpleado")
  private empleado tblEmpleado;
  

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = CifradoUtils.sha256(contrasena);
	}

	public empleado getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(empleado tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}



}
