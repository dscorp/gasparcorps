package model;



import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
@Entity
@Table(name="infocontacto")
public class InfoContacto {
	
	@Id
	@GeneratedValue
	private int idInfoContacto;
	
	@NotEmpty(message= "*El campo direcci�n es requerido")
	@Size(max = 50, message = "El campo direcci�n solo admite 50 caracteres")
	private String direccion;
	
	@NotEmpty(message="*El campo E-mail es requerido")
	@Size(max = 50, message = "El campo email solo admite 50 caracteres")
	@Email(message="El E-Mail no es valido")
	private String email;
	
	
	@NotEmpty(message= "*El campo Telefono es requerido")
	@Size(max = 7, min=7, message = "El campo Telefono es de 7 letras")
	@Pattern(regexp = "^[0-9]*$", message = "El Telefono solo permite Numeros")
	private String telefono;
	
	@NotEmpty(message= "*El campo Celular es requerido")
	@Size(max = 9, min=9, message = "El campo Celular es de 9 letras")
	@Pattern(regexp = "^[0-9]*$", message = "El Celular solo permite Numeros")
	private String celular;


	@OneToOne(mappedBy="tblInfoContacto",cascade=CascadeType.ALL)
	private persona persona;
	
	@OneToOne(mappedBy="tblInfoContacto",cascade= {CascadeType.PERSIST})
	private Proveedor tblProveedor;
	
	
	public int getIdInfoContacto() {
		return idInfoContacto;
	}
	public void setIdInfoContacto(int idInfoContacto) {
		this.idInfoContacto = idInfoContacto;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}




	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public Proveedor getTblProveedor() {
		return tblProveedor;
	}
	public void setTblProveedor(Proveedor tblProveedor) {
		this.tblProveedor = tblProveedor;
	}
	public persona getPersona() {
		return persona;
	}
	public void setPersona(persona persona) {
		this.persona = persona;
	}
	
	
}
