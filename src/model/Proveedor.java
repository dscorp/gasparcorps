package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="proveedor")
public class Proveedor implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int idProveedor;
	
	@NotEmpty(message= "*El campo  RazonSocial es requerido")
//	@Size(max = 25, message = "El campo solo admite 25 letras")
//	@Pattern(regexp = "^[a-zA-Z ]*$", message = "El campo RazonSocial solo permite letras")
	private String razonSocial;

	@NotEmpty(message= "*El campo Ciudad es requerido")
//	@Size(max = 25, message = "El campo solo admite 25 letras")
//	@Pattern(regexp = "^[a-zA-Z ]*$", message = "El campo Ciudad solo permite letras")	
	private String ciudadx;
	
	@NotEmpty(message= "*El campo RUC es requerido")
//	@Size(max = 13, min=13, message = "El campo solo admite 13 letras")
//	@Pattern(regexp = "^[0-9]*$", message = "El campo RUC solo permite Numeros")	
	private String ruc;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	@OneToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST})
	@JoinColumn(name="idInfoContacto")
	private InfoContacto tblInfoContacto;

	
	
	@ManyToMany(mappedBy="proveedores")
	private List<Producto> Productos =  new ArrayList<>();

	
	public int getIdProveedor() {
		return idProveedor;
	}



	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}



	public String getRazonSocial() {
		return razonSocial;
	}



	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}







	public String getCiudadx() {
		return ciudadx;
	}



	public void setCiudadx(String ciudadx) {
		this.ciudadx = ciudadx;
	}





	public String getRuc() {
		return ruc;
	}



	public void setRuc(String ruc) {
		this.ruc = ruc;
	}



	public Date getFechaRegistro() {
		return fechaRegistro;
	}



	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}



	public InfoContacto getTblInfoContacto() {
		return tblInfoContacto;
	}



	public void setTblInfoContacto(InfoContacto tblInfoContacto) {
		this.tblInfoContacto = tblInfoContacto;
	}




	public List<Producto> getProductos() {
		return Productos;
	}



	public void setProductos(List<Producto> productos) {
		Productos = productos;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

 	

	
	
}
