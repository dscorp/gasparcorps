package model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="entradaProducto")
public class EntradaProducto 
{
	@Id
	@GeneratedValue
	private int idEntradaProducto;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEntrada;
	
	@ManyToOne()
	@JoinColumn(name="idEmpleado")
	private empleado tblEmpleado;
	
	@OneToMany(mappedBy="tblEntPro",cascade = {CascadeType.PERSIST})
	private List<DetalleEntradaProducto> tblDetEntPro;

	public int getIdEntradaProducto() {
		return idEntradaProducto;
	}

	public void setIdEntradaProducto(int idEntradaProducto) {
		this.idEntradaProducto = idEntradaProducto;
	}

	public Date getFechaEntrada() {
		return fechaEntrada;
	}

	public void setFechaEntrada(Date fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}

	public empleado getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(empleado tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}

	public List<DetalleEntradaProducto> getTblDetEntPro() {
		return tblDetEntPro;
	}

	public void setTblDetEntPro(List<DetalleEntradaProducto> tblDetEntPro) {
		this.tblDetEntPro = tblDetEntPro;
	}
	
	
	
}
