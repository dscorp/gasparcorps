package model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="empleado")
public class empleado 
{
  @Id
  @GeneratedValue
  private int idEmpleado;
  @NotNull(message="El campo FechaContrato es requerido")
  @Temporal(TemporalType.TIMESTAMP)
  private Date fechaContrato;
  
  
  @OneToOne(mappedBy = "tblEmpleado")
	private Usuario tblUsuario;
  
  @OneToMany(mappedBy="tblEmpleado",cascade= CascadeType.MERGE)
  private List<EntradaProducto> tblEntPro;
  
  @OneToMany(mappedBy="tblEmpleado")
  private List<SalidaProducto> tblSalPro;
  
  
  @NotNull(message="Debe seleccionar un cargo")
  @ManyToOne(cascade=CascadeType.MERGE)
  @JoinColumn(name="idCargo")
  private Cargo tblcargo;
  
  
  @OneToOne(cascade=CascadeType.MERGE)
  @JoinColumn(name = "dniPersona")
  private persona tblpersona;

  
  
public int getIdEmpleado() {
	return idEmpleado;
}

public void setIdEmpleado(int idEmpleado) {
	this.idEmpleado = idEmpleado;
}

public Date getFechaContrato() {
	return fechaContrato;
}

public void setFechaContrato(Date fechaContrato) {
	this.fechaContrato = fechaContrato;
}
//
//public usuario getTblusuario() {
//	return tblusuario;
//}
//
//public void setTblusuario(usuario tblusuario) {
//	this.tblusuario = tblusuario;
//}
//


public persona getTblpersona() {
	return tblpersona;
}

public void setTblpersona(persona tblpersona) {
	this.tblpersona = tblpersona;
}

public Cargo getTblcargo() {
	return tblcargo;
}

public void setTblcargo(Cargo tblcargo) {
	this.tblcargo = tblcargo;
}

public Usuario getTblUsuario() {
	return tblUsuario;
}

public void setTblUsuario(Usuario tblUsuario) {
	this.tblUsuario = tblUsuario;
}

public List<EntradaProducto> getTblEntPro() {
	return tblEntPro;
}

public void setTblEntPro(List<EntradaProducto> tblEntPro) {
	this.tblEntPro = tblEntPro;
}

public List<SalidaProducto> getTblSalPro() {
	return tblSalPro;
}

public void setTblSalPro(List<SalidaProducto> tblSalPro) {
	this.tblSalPro = tblSalPro;
}



  
  
}
