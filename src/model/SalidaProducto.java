package model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="salidaProducto")
public class SalidaProducto 
{
	@Id
	@GeneratedValue
	private int idSalidaProducto;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaSalida;
	
	@ManyToOne()
	@JoinColumn(name="idEmpleado")
	private empleado tblEmpleado;
	
	@OneToMany(mappedBy="tblSalPro", cascade = {CascadeType.PERSIST}, fetch=FetchType.EAGER)
	private List<DetalleSalidaProducto> tblDetSalPro;

	public int getIdSalidaProducto() {
		return idSalidaProducto;
	}

	public void setIdSalidaProducto(int idSalidaProducto) {
		this.idSalidaProducto = idSalidaProducto;
	}

	public Date getFechaSalida() {
		return fechaSalida;
	}

	public void setFechaSalida(Date fechaSalida) {
		this.fechaSalida = fechaSalida;
	}

	public empleado getTblEmpleado() {
		return tblEmpleado;
	}

	public void setTblEmpleado(empleado tblEmpleado) {
		this.tblEmpleado = tblEmpleado;
	}

	public List<DetalleSalidaProducto> getTblDetSalPro() {
		return tblDetSalPro;
	}

	public void setTblDetSalPro(List<DetalleSalidaProducto> tblDetSalPro) {
		this.tblDetSalPro = tblDetSalPro;
	}
	
	
}
