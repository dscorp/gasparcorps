package model;

import java.math.BigDecimal;

import javax.enterprise.inject.New;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import jdk.nashorn.internal.ir.annotations.Ignore;
import services.ProductoService;


@Entity
@Table(name="detalleEntradaProducto")
public class DetalleEntradaProducto 
{
	
	@Id
	@GeneratedValue
	private int idDetalleEntradaProducto;
	
	@NotNull(message = "*El campo cantidad es requerido")
	@Digits(integer=3,  fraction = 2,message="La cantidad puede contener 3 enteros y 2 decimales como maximo")
	private double cantidad;
	
	@ManyToOne()
	@JoinColumn(name="idProducto")
	private Producto tblProducto;
	
	@ManyToOne()
	@JoinColumn(name="idEntradaProducto")
	private EntradaProducto tblEntPro;


	public int getIdDetalleEntradaProducto() {
		return idDetalleEntradaProducto;
	}

	public void setIdDetalleEntradaProducto(int idDetalleEntradaProducto) {
		this.idDetalleEntradaProducto = idDetalleEntradaProducto;
	}


	

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public Producto getTblProducto() {
		return tblProducto;
	}

	public void setTblProducto(Producto tblProducto) {
		this.tblProducto = tblProducto;
	}

	public EntradaProducto getTblEntPro() {
		return tblEntPro;
	}

	public void setTblEntPro(EntradaProducto tblEntPro) {
		this.tblEntPro = tblEntPro;
	}
	
	
}
