package model;



import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="categoria")
public class Categoria 
{
	@Id
	@GeneratedValue
	private int idCategoria;
	
	@NotEmpty(message= "*El campo de categoria es requerido")
	@Size(max = 25, message = "El campo categoria solo admite 25 letras")
	@Pattern(regexp = "^[a-zA-Z ]*$", message = "El campo categoria solo permite letras")
	private String nombre;

	@OneToMany(mappedBy="tblCategoria")
	private List<Producto> tblProducto;

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Producto> getTblProducto() {
		return tblProducto;
	}

	public void setTblProducto(List<Producto> tblProducto) {
		this.tblProducto = tblProducto;
	}

	@Override
	public String toString() {
		return "Categoria [idCategoria=" + idCategoria + ", nombre=" + nombre + ", tblProducto=" + tblProducto + "]";
	}


	
	
}
