package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;



@Entity
@Table(name="producto")
public class Producto implements Serializable
{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="idProducto")
	private int idProducto;
	
	@NotNull(message = "*El campo CodigoBarrar es requerido")
	@Size(min = 13 , max=13, message = "El codigo debe ser de 13 digitos")
	@Column(unique=true)
	private String codigoBarra;
	
	
	@NotEmpty(message= "*El campo de Nombre es requerido")
	@Size(max = 25, message = "El campo solo admite 25 letras")
	@Pattern(regexp = "^[a-zA-Z ]*$", message = "El nombre solo permite letras")
	private String nombre;
	
	@NotEmpty(message="*El campo Marca es requerido")
	@Size(max= 20 , message = "El campo solo admite 20 letras")
	@Pattern(regexp = "^[a-zA-Z ]*$", message = "La marca solo admite letras")
	private String marca;
	
	
	@NotNull(message = "*El campo Stock es requerido")
//	@Digits(integer=3, fraction = 2,message="El stock puede contener 3 enteros y 2 decimales como maximo")
	private BigDecimal stock;
	
	@NotEmpty(message="*El campo UnidadMedida es requerido")
	@Pattern(regexp = "^[a-zA-Z ]*$", message = "La UnidadMedida solo acepta letras")
	private String unidadMedida;
	
	 @Temporal(TemporalType.TIMESTAMP)
	  private Date fechaRegistro;

	
	@OneToMany(mappedBy="tblProducto",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
	private List<DetalleProducto> tblDetPro; 
	
	@OneToMany(mappedBy="tblProducto")
	private List<DetalleEntradaProducto> tblDetEntPro;
	
	@OneToMany(mappedBy="tblProducto",cascade= {CascadeType.REMOVE,CascadeType.MERGE})
	private List<DetalleSalidaProducto> tblDetSalPro;
	
	@ManyToOne()
	@JoinColumn(name="idCategoria")
	private Categoria tblCategoria;

	
	
	 @ManyToMany(fetch=FetchType.EAGER)
	 private List<Proveedor> proveedores  = new ArrayList<>();
	 		
	public Producto() {
	
	}

	
	
	
	

	public Producto(
		String nombre,String marca,
			 BigDecimal stock) {
		
		this.nombre = nombre;
		this.marca = marca;
		this.stock = stock;
	}
















	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getCodigoBarra() {
		return codigoBarra;
	}

	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}



	public BigDecimal getStock() {
		return stock;
	}


	public void setStock(BigDecimal stock) {
		this.stock = stock;
	}


	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	

	public List<DetalleProducto> getTblDetPro() {
		return tblDetPro;
	}

	public void setTblDetPro(List<DetalleProducto> tblDetPro) {
		this.tblDetPro = tblDetPro;
	}

	public Categoria getTblCategoria() {
		return tblCategoria;
	}

	public void setTblCategoria(Categoria tblCategoria) {
		this.tblCategoria = tblCategoria;
	}


	public Date getFechaRegistro() {
		return fechaRegistro;
	}


	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}


	


	public List<DetalleEntradaProducto> getTblDetEntPro() {
		return tblDetEntPro;
	}


	public void setTblDetEntPro(List<DetalleEntradaProducto> tblDetEntPro) {
		this.tblDetEntPro = tblDetEntPro;
	}


	public List<DetalleSalidaProducto> getTblDetSalPro() {
		return tblDetSalPro;
	}


	public void setTblDetSalPro(List<DetalleSalidaProducto> tblDetSalPro) {
		this.tblDetSalPro = tblDetSalPro;
	}



	public List<Proveedor> getProveedores() {
		return proveedores;
	}






	public void setProveedores(List<Proveedor> proveedores) {
		this.proveedores = proveedores;
	}






	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	

	
}
